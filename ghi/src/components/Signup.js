import React, { useState } from 'react'
import ReactModal from 'react-modal'
import { useNavigate } from 'react-router-dom'
import '../css/styles.css'
import useToken from "@galvanize-inc/jwtdown-for-react";
const baseUrl = process.env.REACT_APP_API_HOST

export default function Signup() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const [error, setError] = useState(null)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const navigate = useNavigate()
  const { login } = useToken();

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await fetch(`${baseUrl}/api/accounts`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username, password, email })
      })

      const responseData = await response.json();  // Get the JSON response regardless of success or failure

      if (response.ok) {
        login(username, password);
        navigate('/user/profile')
      } else {
        console.log(responseData)
        if (responseData.detail === "Email already exists") {
          setError("The email you entered is already associated with another account.");
        } else if (responseData.detail === "Username already exists") {
          setError("The username you chose is already taken.");
        } else {
          setError("An unknown error occurred. Please try again.");
        }
        setIsModalOpen(true);
      }
    } catch (error) {
      console.error('Error signing up:', error)
    }
  }

  const closeModal = () => {
    setIsModalOpen(false)
    setError('')
  }

  return (
    <div className='container mt-5 pt-5'>
      <div className="row my-5">
        <div className="offset-3 col-6">
          <form className="signup-form" onSubmit={handleSubmit}>
            <h1>Sign Up</h1>
            <div className="mb-3" >
              <label htmlFor="username">Username</label>
              <input className="form-control" placeholder="username" type="text" id="username" value={username} onChange={(e) => setUsername(e.target.value)} />
            </div>
            <div className="mb-3" >
              <label htmlFor="password">Password</label>
              <input className="form-control" placeholder="password" type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </div>
            <div className="mb-3">
              <label className="form-label" htmlFor="email">Email</label>
              <input className="form-control" placeholder="email" type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </div>
            <button type="submit">Sign Up</button>
          </form>
        </div>
        <div>
          <ReactModal
            ariaHideApp={false}
            isOpen={isModalOpen}
            onRequestClose={closeModal}
            contentLabel="Sign Up Failed"
            style={{
              overlay: {
                position: 'fixed',
                backgroundColor: 'rgba(0, 0, 0, 0.75)'
              }, content: {
                position: 'absolute',
                width: '400px',
                height: '200px',
                left: '50%',
                top: '50%',
                transform: 'translate(-50%, -50%)',
                background: '#7393B3',
                textAlign: 'center',
                border: '1px solid #ccc',
                borderRadius: '10px'
              }
            }}>
            <h2>Uh-Oh! Sign Up Failed. </h2>
            <p>{error}</p>
            <button onClick={closeModal}>Close</button>
          </ReactModal>
        </div>
      </div>
    </div>
  )
}
